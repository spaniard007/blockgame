﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UI_Manager : MonoBehaviour {

	public Text scoreText; 
	public Text hiScoreText;
	// Use this for initialization
	void Start () {
		//PlayerPrefs.SetInt ("HighScore",0);
	}
	
	// Update is called once per frame
	void Update () {
		scoreText.text = "Score : " + GameManager.score.ToString ();
		hiScoreText.text = "High Score : "+PlayerPrefs.GetInt ("HighScore").ToString(); 
	}

	public void StartGame()
	{
		SceneManager.LoadScene (1);
	}
}
