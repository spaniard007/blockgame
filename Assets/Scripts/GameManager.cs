﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public static int score = 0;
	public static bool isGameOver = false;

	public Text ScoreText;
	public static bool isGameStarted = false ;

	// Use this for initialization
	void Start () {
		score = 0;
		isGameOver = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (!isGameOver) {
			score += (int)(Time.timeScale);
			Debug.Log (score);
		} else {
			Debug.Log ("Chill");
			GameOver ();
		}
		ScoreText.text = score.ToString ();
	}



	public void GameOver()
	{
		
		if (score > PlayerPrefs.GetInt ("HighScore")) {
			PlayerPrefs.SetInt ("HighScore",score);
		}
		SceneManager.LoadScene (0);

		//GameOverPanel.gameObject.SetActive (true);
		//isGameStarted = false;
		//ResetGameData();
		//NewGame();
	}




	public void ResetGameData()
	{
		score = 0;
		//Time.time = 0.0f;
	}
}
