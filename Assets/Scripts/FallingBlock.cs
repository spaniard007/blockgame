﻿using UnityEngine;
using System.Collections;

public class FallingBlock : MonoBehaviour {

	public Vector2 speedMinMax;
	float speed;
	public float destroytime = 3f;

	void Start() {
		speed = Mathf.Lerp (speedMinMax.x, speedMinMax.y, Difficulty.GetDifficultyPercent ());
		Invoke ("DestroyObstacles",destroytime);
	}

	void Update () {
		Debug.Log ("Diff :" + Difficulty.GetDifficultyPercent () );
		transform.Translate (Vector3.down * speed * Time.deltaTime, Space.Self);
	}

	public void DestroyObstacles()
	{
		Destroy (this.gameObject);
	}
}
